package com.trygle.robotdemo;

import java.io.IOException;
import java.nio.FloatBuffer;
import java.util.ArrayList;

import javax.microedition.khronos.opengles.GL10;
import android.content.Context;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.opengl.GLES11Ext;

public class CameraController extends GLObject implements Camera.PictureCallback {
    protected Camera mCamera;
    protected SurfaceTexture mSurfaceTexture;

    private final ArrayList<FloatBuffer> mVertexBuffer = new ArrayList<FloatBuffer>();
    private final ArrayList<FloatBuffer> mNormalBuffer = new ArrayList<FloatBuffer>();
    private final ArrayList<FloatBuffer> mTextureBuffer = new ArrayList<FloatBuffer>();
    protected final int[] mTextures = new int[1];

    public CameraController(Context context) {
        createModel(1.0f);
    }

    @Override
    public void onPictureTaken(byte[] data, Camera c) {
        mCamera.startPreview();
    }

    private void createModel(final float width) {
        final float[] vertices = new float[4 * 3];
        final float[] normals = new float[4 * 3];
        final float[] texturePoints = new float[4 * 2];
        int vertexPos = 0;
        int normalPos = 0;
        int texturePos = 0;
        for (int i = 0; i < 4; i++) {
            float x = i % 2;
            float y = i / 2;
            vertices[vertexPos++] = x * 2 - 1;
            vertices[vertexPos++] = y * 2 - 1;
            vertices[vertexPos++] = 0;
            normals[normalPos++] = 0;
            normals[normalPos++] = 0;
            normals[normalPos++] = -1;
            texturePoints[texturePos++] = x;
            texturePoints[texturePos++] = 1 - y;
        }

        addFloatBuffer(mVertexBuffer, vertices);
        addFloatBuffer(mNormalBuffer, normals);
        addFloatBuffer(mTextureBuffer, texturePoints);
    }

    public void initialize(final GL10 gl) {
        gl.glGenTextures(1, mTextures, 0);
    }

    public void startPreview() {
        mSurfaceTexture = new SurfaceTexture(mTextures[0]);

        try {
            mCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_FRONT);
            Camera.Parameters parameters = mCamera.getParameters();
            parameters.setPreviewSize(640, 480);
            mCamera.setParameters(parameters);
            mCamera.setPreviewTexture(mSurfaceTexture);
            mCamera.startPreview();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void stopPreview() {
        synchronized (this) {
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;

            mSurfaceTexture = null;
        }
    }

    public void takePicture() {
        mCamera.takePicture(null, null, this);
    }

    public void draw(final GL10 gl) {
        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
        gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);

        gl.glEnable(GLES11Ext.GL_TEXTURE_EXTERNAL_OES);
        gl.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, mTextures[0]);

        synchronized (this) {
            if (mSurfaceTexture != null) {
                mSurfaceTexture.updateTexImage();
            }
        }

        gl.glPushMatrix();
        applyTransformation(gl);

        for (int i = 0; i < mVertexBuffer.size(); i++) {
            gl.glVertexPointer(3, GL10.GL_FLOAT, 0, mVertexBuffer.get(i));
            gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, mTextureBuffer.get(i));
            gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, mVertexBuffer.get(i).capacity() / 3);
        }

        gl.glPopMatrix();

        gl.glDisable(GLES11Ext.GL_TEXTURE_EXTERNAL_OES);

        gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
        gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
    }
}
