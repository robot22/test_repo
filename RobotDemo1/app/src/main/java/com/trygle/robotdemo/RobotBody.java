package com.trygle.robotdemo;

import java.lang.reflect.Field;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import android.opengl.GLSurfaceView;
import android.os.Handler;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Toast;

import com.panasonic.three.HTTPClient;
import com.panasonic.three.MyCameraController;

public class RobotBody {
    private static final boolean REV = false;
    
    private final Context mContext;
    private final Handler mHandler;

    private CameraController mCameraController = null;

    private ServoController mServoController = null;

    private GLRenderer mGLRenderer = null;

    private final EyeBall mEyeBall;
    private final EyeBallLighting mEyeBallLighting;
    private final TexturePlain mCameraLens;
    private final TexturePlain mStopWatchBg;
    private final TexturePlain mStopWatchHand;

    //TESTTEST
    private final RekognitionInfoDrawer mRekognitionInfoDrawer;// 20150715 hattori add

    private final SoundPool mSoundPool;
    private final int  mSoundAlarm;
    private final int mSoundShutter;

    private Animator mStopWatchAnimator = null;
    private int mDegree1 = 0;
    private int mDegree2 = REV ? 18000 : 0;

    private String mServoAddress = "192.168.43.184:8081";

    public RobotBody(Context context) {
        mContext = context;
        mHandler = new Handler();

        mCameraController = new MyCameraController(context);
        mEyeBall = new EyeBall(5);
        mEyeBallLighting = new EyeBallLighting();
        mCameraLens = new TexturePlain(context, R.drawable.lens);
        mStopWatchBg = new TexturePlain(context, R.drawable.stopwatch_bg);
        mStopWatchHand = new TexturePlain(context, R.drawable.stopwatch_hand);

        mRekognitionInfoDrawer = new RekognitionInfoDrawer(context);// 20150715 hattori add

        mGLRenderer = new GLRenderer();
        mGLRenderer.addObject(mEyeBall);
        mGLRenderer.addObject(mEyeBallLighting);
        mGLRenderer.addObject(mCameraLens);
        mGLRenderer.addObject(mStopWatchBg);
        mGLRenderer.addObject(mStopWatchHand);
        mGLRenderer.addObject(mCameraController);

       mGLRenderer.addObject(mRekognitionInfoDrawer);// 20150715 hattori add

        try {
            mServoController = new ServoController(context);
        } catch (Exception e) {
            mServoController = null;
            Toast.makeText(context, "サーボコントローラの初期化に失敗しました", Toast.LENGTH_SHORT).show();
        }

        int streamType = AudioManager.STREAM_SYSTEM;
        try {
            Class<?> asClass = Class.forName("android.media.AudioSystem");
            Field sseField = asClass.getDeclaredField("STREAM_SYSTEM_ENFORCED");
            streamType = sseField.getInt(null);
        } catch (Exception e) {

        }
        mSoundPool = new SoundPool(2, streamType, 0);

        mSoundAlarm = mSoundPool.load(context, R.raw.alarm, 1);
        mSoundShutter = mSoundPool.load(context, R.raw.shutter, 1);
    }

    public void onResume() {
        mServoController.onResume();
        initializeParameter();
        initializeServoController();
    }

    public void onPause() {
        mServoController.onPause();
    }

    public GLSurfaceView.Renderer getRenderer() {
        return mGLRenderer;
    }

    public void initializeParameter() {
        mCameraController.setTranslateX(-2.5f);
        mCameraController.setScaleY(0.75f);
        mCameraController.setTranslateY(-3f);
        mCameraLens.setTranslateY(5f);
        mStopWatchBg.setTranslateY(5f);
        mStopWatchHand.setTranslateY(5f);
        mStopWatchBg.setScaleX(1.2f);
        mStopWatchBg.setScaleY(1.2f);
        mStopWatchHand.setScaleX(1.2f);
        mStopWatchHand.setScaleY(1.2f);

        mRekognitionInfoDrawer.setScaleX(0.5f);// 20150715 hattori add
        mRekognitionInfoDrawer.setScaleY(0.5f);// 20150715 hattori add
    }

    public void dontknow() {
        Animator eyeAnim = ObjectAnimator.ofPropertyValuesHolder(mEyeBall,
                PropertyValuesHolder.ofFloat("translateX", 0.0f, 0.5f, -0.5f, 0.5f, 0.0f));
        eyeAnim.setInterpolator(new LinearInterpolator());
        eyeAnim.setDuration(2000);
        eyeAnim.start();
    }

    public void blink() {
        Animator eyeAnim = ObjectAnimator.ofPropertyValuesHolder(mEyeBall,
                PropertyValuesHolder.ofFloat("scaleY", 1,0f, 0.2f, 1.0f),
                PropertyValuesHolder.ofFloat("translateY", 0.0f, -0.5f, 0.0f));
        eyeAnim.setInterpolator(new LinearInterpolator());
        eyeAnim.setDuration(500);
        eyeAnim.start();
    }

    public void nod() {
        Animator eyeAnim = ObjectAnimator.ofPropertyValuesHolder(mEyeBall,
                PropertyValuesHolder.ofFloat("scaleY", 1.0f, 0.9f, 1.0f, 0.9f, 1.0f),
                PropertyValuesHolder.ofFloat("translateY", 0.0f, -0.1f, 0.0f, -0.1f, 0.0f));
        eyeAnim.setInterpolator(new LinearInterpolator());
        eyeAnim.setDuration(800);
        eyeAnim.start();
    }

    public void greetings() {
        Animator eyeAnim = ObjectAnimator.ofPropertyValuesHolder(mEyeBall,
                PropertyValuesHolder.ofFloat("scaleY", 1.0f, 0.4f, 1.0f),
                PropertyValuesHolder.ofFloat("rotationZ", 0.0f, REV ? 30.0f : -30.0f, 0.0f),
                PropertyValuesHolder.ofFloat("translateY", 0.0f, -0.5f, 0.0f));
        eyeAnim.setInterpolator(new LinearInterpolator());
        eyeAnim.setDuration(1500);
        eyeAnim.start();

        //new HTTPClient("http://" + mServoAddress + "/action/robot/greetings").post("");

        rotateBody2(0, +9000, +9000, 750);
        rotateBody2(750, -9000, -9000, 750);
    }

    public void smile() {
        Animator beginAnim = ObjectAnimator.ofPropertyValuesHolder(mEyeBallLighting,
                PropertyValuesHolder.ofFloat("lighting", 0,0f, 1.0f));
        Animator eyeAnim = ObjectAnimator.ofPropertyValuesHolder(mEyeBall,
                PropertyValuesHolder.ofFloat("translateY", 0.0f, -0.2f, 0.0f, -0.2f, 0.0f, -0.2f, 0.0f, -0.2f, 0.0f));
        Animator endAnim = ObjectAnimator.ofPropertyValuesHolder(mEyeBallLighting,
                PropertyValuesHolder.ofFloat("lighting", 1,0f, 0.0f));
        beginAnim.setDuration(300);
        eyeAnim.setInterpolator(new LinearInterpolator());
        eyeAnim.setDuration(1500);
        endAnim.setDuration(300);

        AnimatorSet animSet = new AnimatorSet();
        animSet.play(eyeAnim).after(beginAnim).before(endAnim);
        animSet.start();

        new HTTPClient("http://" + mServoAddress + "/action/robot/smile").post("");

        /*rotateBody2(   0, +2000, +2000, 250);
        rotateBody2( 250, -4000, -4000, 400);
        rotateBody2( 650, +4000, +4000, 400);
        rotateBody2(1050, -4000, -4000, 400);
        rotateBody2(1450, +2000, +2000, 250);*/
    }

    private void rotateBody(long delay, final int id, final int degree, final int speed) {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (id == 1) {
                    mDegree1 += degree;
                    mServoController.setPosition(1, mDegree1);
                } else {
                    mDegree2 += degree;
                    mServoController.setPosition(2, mDegree2);
                }
            }
        }, delay);
    }

    private void rotateBody2(long delay, final int deg1, final int deg2, final int duration) {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mDegree1 += deg1;
                mDegree2 += deg2;

                new HTTPClient("http://" + mServoAddress + "/action/robot/rotate?0&" + deg1 + "&" + deg2 + "&" + duration).post("");

                mServoController.setPositions(mDegree1, mDegree2, duration);
            }
        }, delay);
    }

    public void reset() {

        //new HTTPClient("http://" + mServoAddress + "/action/robot/reset").post("");

        rotateBody2(   0,     0, +3000, 300);
        rotateBody2(300, -3000, -3000, 300);
    }


    public void grungrun() {

        //new HTTPClient("http://" + mServoAddress + "/action/robot/reset").post("");

        rotateBody2(   0,     -18000, +18000, 1500);
        rotateBody2(   1500,     18000, -18000, 1500);
        //rotateBody2( 300, -3000, -3000, 300);
    }


    public void lookLeft() {

        //new HTTPClient("http://" + mServoAddress + "/action/robot/lookLeft").post("");

        rotateBody2(   0,     0, +3000, 300);
        rotateBody2( 300, -3000, -3000, 300);
    }

    public void lookRight() {

        //new HTTPClient("http://" + mServoAddress + "/action/robot/lookRight").post("");

        rotateBody2(   0,     0, -3000, 300);
        rotateBody2( 300, +3000, +3000, 300);
    }

    public void dangerous() {

        Animator eyeAnim = ObjectAnimator.ofPropertyValuesHolder(mEyeBall,
                PropertyValuesHolder.ofFloat("scaleX", 1.0f, 0.3f, 1.0f),
                PropertyValuesHolder.ofFloat("scaleY", 1.0f, 0.3f, 1.0f),
                PropertyValuesHolder.ofFloat("scaleZ", 1.0f, 0.3f, 1.0f));
        eyeAnim.setDuration(500);
        eyeAnim.start();

        //new HTTPClient("http://" + mServoAddress + "/action/robot/dangerous").post("");

        rotateBody2(500, 0, -12000, 500);
        rotateBody2(2000, 0, +12000, 1500);
    }

    public void cameraStandBy() {
        Animator lensAnim = ObjectAnimator.ofPropertyValuesHolder(mCameraLens,
                PropertyValuesHolder.ofFloat("translateZ", 1.0f, 1.0f),
                PropertyValuesHolder.ofFloat("RotationY", -180.0f, 0.0f),
                PropertyValuesHolder.ofFloat("translateY", 0.0f, 0.0f));
        Animator prevAnim = ObjectAnimator.ofPropertyValuesHolder(mCameraController,
                PropertyValuesHolder.ofFloat("translateY", -3.0f, 0.0f));
        Animator eyeAnim = ObjectAnimator.ofPropertyValuesHolder(mEyeBall,
                PropertyValuesHolder.ofFloat("scaleX", 1.0f, 0.5f, 1.0f, 0.5f, 1.0f),
                PropertyValuesHolder.ofFloat("scaleY", 1.0f, 0.5f, 1.0f, 0.5f, 1.0f),
                PropertyValuesHolder.ofFloat("scaleZ", 1.0f, 0.5f, 1.0f, 0.5f, 1.0f));
        lensAnim.setDuration(500);
        lensAnim.setInterpolator(new LinearInterpolator());
        prevAnim.setDuration(700);
        eyeAnim.setInterpolator(new LinearInterpolator());
        eyeAnim.setDuration(300);

        AnimatorSet animSet = new AnimatorSet();
        animSet.play(lensAnim).with(prevAnim).before(eyeAnim);
        animSet.start();

        mCameraController.startPreview();
    }

    public void cameraOut() {
        Animator lensAnim = ObjectAnimator.ofPropertyValuesHolder(mCameraLens,
                PropertyValuesHolder.ofFloat("translateZ", 1.0f, 1.0f),
                PropertyValuesHolder.ofFloat("RotationY", 0.0f, 180.0f),
                PropertyValuesHolder.ofFloat("translateY", 0.0f, 0.0f));
        Animator prevAnim = ObjectAnimator.ofPropertyValuesHolder(mCameraController,
                PropertyValuesHolder.ofFloat("translateY", 0.0f, -3.0f));
        lensAnim.setInterpolator(new LinearInterpolator());
        lensAnim.setDuration(500);
        prevAnim.setDuration(700);

        AnimatorSet animSet = new AnimatorSet();
        animSet.play(lensAnim).with(prevAnim);
        animSet.addListener(new AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                onAnimationEnd(animation);
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                mCameraLens.setTranslateY(5f);
            }
        });
        animSet.start();
        mCameraController.stopPreview();
    }

    public void takePicture() {
        mCameraController.takePicture();
        Animator lensAnim = ObjectAnimator.ofPropertyValuesHolder(mEyeBall,
                PropertyValuesHolder.ofFloat("scaleX", 1.0f, 0.3f, 1.0f),
                PropertyValuesHolder.ofFloat("scaleY", 1.0f, 0.3f, 1.0f),
                PropertyValuesHolder.ofFloat("scaleZ", 1.0f, 0.3f, 1.0f));
        Animator prevAnim1 = ObjectAnimator.ofPropertyValuesHolder(mCameraController,
                PropertyValuesHolder.ofFloat("translateX", -2.5f, -4.0f));
        Animator prevAnim2 = ObjectAnimator.ofPropertyValuesHolder(mCameraController,
                PropertyValuesHolder.ofFloat("translateY", 0.0f, -4.0f));
        Animator prevAnim3 = ObjectAnimator.ofPropertyValuesHolder(mCameraController,
                PropertyValuesHolder.ofFloat("scaleX", 1.0f, 0.1f),
                PropertyValuesHolder.ofFloat("scaleY", 0.75f, 0));
        lensAnim.setInterpolator(new LinearInterpolator());
        lensAnim.setDuration(300);
        prevAnim1.setDuration(1000);
        prevAnim1.setInterpolator(new AccelerateInterpolator());
        prevAnim2.setDuration(1000);
        prevAnim2.setInterpolator(new AnticipateInterpolator());
        prevAnim3.setDuration(1000);
        prevAnim3.setInterpolator(new AccelerateInterpolator());

        AnimatorSet animSet = new AnimatorSet();
        animSet.play(prevAnim1).with(prevAnim2).with(prevAnim3).after(lensAnim);
        animSet.addListener(new AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                onAnimationEnd(animation);
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                mCameraController.setTranslateX(-2.5f);
                mCameraController.setTranslateY(0.0f);
                mCameraController.setScaleX(1.0f);
                mCameraController.setScaleY(0.75f);
            }
        });
        animSet.start();

        mSoundPool.play(mSoundShutter, 1.0f, 1.0f, 0, 0, 1.0f);
    }

    public void startWatch(final int sec) {
        Animator beginAnim1 = ObjectAnimator.ofPropertyValuesHolder(mStopWatchBg,
                PropertyValuesHolder.ofFloat("translateZ", 1.0f, 1.0f),
                PropertyValuesHolder.ofFloat("rotationY", -180.0f, 0.0f),
                PropertyValuesHolder.ofFloat("translateY", 0.0f, 0.0f));
        Animator beginAnim2 = ObjectAnimator.ofPropertyValuesHolder(mStopWatchHand,
                PropertyValuesHolder.ofFloat("translateZ", 1.01f, 1.01f),
                PropertyValuesHolder.ofFloat("rotationY", -180.0f, 0.0f),
                PropertyValuesHolder.ofFloat("translateY", 0.0f, 0.0f));
        Animator beginAnim3 = ObjectAnimator.ofPropertyValuesHolder(mEyeBall,
                PropertyValuesHolder.ofFloat("translateZ", 0f, -1.0f));
        beginAnim1.setDuration(500);
        beginAnim1.setInterpolator(new LinearInterpolator());
        beginAnim2.setDuration(500);
        beginAnim2.setInterpolator(new LinearInterpolator());

        final AnimatorSet animSet = new AnimatorSet();
        animSet.play(beginAnim1).with(beginAnim2).with(beginAnim3);
        animSet.addListener(new AnimatorSet.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {}
            @Override
            public void onAnimationCancel(Animator animation) { onAnimationEnd(animation); }
            @Override
            public void onAnimationRepeat(Animator animation) {}
            @Override
            public void onAnimationEnd(Animator animation) {
                resetWatch(sec);
            }
        });
        animSet.start();
    }

    public void resetWatch(final int sec) {
        synchronized (this) {
            if (mStopWatchAnimator != null) {
                mStopWatchAnimator.cancel();
            }
        }
        synchronized (this) {
            mStopWatchAnimator = ObjectAnimator.ofPropertyValuesHolder(mStopWatchHand,
                    PropertyValuesHolder.ofFloat("rotationZ", -sec * 6f, 0f));
            mStopWatchAnimator.setInterpolator(new LinearInterpolator());
            mStopWatchAnimator.setDuration(sec * 1000);
            mStopWatchAnimator.addListener(new AnimatorSet.AnimatorListener() {
                private boolean mCancelled = false;
                @Override
                public void onAnimationStart(Animator animation) {}
                @Override
                public void onAnimationCancel(Animator animation) {
                    mCancelled = true;
                }
                @Override
                public void onAnimationRepeat(Animator animation) {}
                @Override
                public void onAnimationEnd(Animator animation) {
                    synchronized (RobotBody.this) {
                        mStopWatchAnimator = null;
                    }
                    if (!mCancelled) {
                        vibrateWatch();
                    }
                }
            });
            mStopWatchAnimator.start();
        }
    }

    public void vibrateWatch() {
        Animator watchAnim = ObjectAnimator.ofPropertyValuesHolder(mStopWatchBg,
                PropertyValuesHolder.ofFloat("translateX", 0.0f, -0.1f, 0.1f, -0.1f, 0.1f, -0.1f, 0.1f, -0.1f, 0.1f, -0.1f, 0.1f, -0.1f, 0.1f, 0.0f));
        watchAnim.setDuration(500);
        watchAnim.start();

        mSoundPool.play(mSoundAlarm, 1.0f, 1.0f, 0, 0, 1.0f);
    }

    public void stopWatch() {
        Animator endAnim1 = ObjectAnimator.ofPropertyValuesHolder(mStopWatchBg,
                PropertyValuesHolder.ofFloat("translateZ", 1.0f, 1.0f),
                PropertyValuesHolder.ofFloat("rotationY",  0.0f, 180.0f),
                PropertyValuesHolder.ofFloat("translateY", 0.0f, 0.0f));
        Animator endAnim2 = ObjectAnimator.ofPropertyValuesHolder(mStopWatchHand,
                PropertyValuesHolder.ofFloat("translateZ", 1.01f, 1.01f),
                PropertyValuesHolder.ofFloat("rotationY",  0.0f, 180.0f),
                PropertyValuesHolder.ofFloat("translateY", 0.0f, 0.0f));
        Animator endAnim3 = ObjectAnimator.ofPropertyValuesHolder(mEyeBall,
                PropertyValuesHolder.ofFloat("translateZ", -1.0f, 0f));

        AnimatorSet animSet = new AnimatorSet();
        animSet.play(endAnim1).with(endAnim2).with(endAnim3);
        animSet.addListener(new AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                onAnimationEnd(animation);
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                mStopWatchBg.setTranslateY(5f);
                mStopWatchHand.setTranslateY(5f);
            }
        });
        animSet.start();
    }

    private void initializeServoController() {
        mServoController.setTrajectory(1, 5);
        mServoController.setTrajectory(2, 5);
        mServoController.initialize(1);
        mServoController.initialize(2);
        mServoController.setPosition(1, mDegree1);
        mServoController.setPosition(2, mDegree2);
    }

    // TESTTEST
    public void sp()
    {

        mRekognitionInfoDrawer.switchPreview();
    }

}
