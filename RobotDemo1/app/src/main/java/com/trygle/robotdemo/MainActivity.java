package com.trygle.robotdemo;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.content.Context;
import android.opengl.GLSurfaceView;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

public class MainActivity extends FragmentActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        final View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if ((visibility & View.SYSTEM_UI_FLAG_HIDE_NAVIGATION) == 0) {
                    decorView.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            MainActivity.this.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
                        }
                    }, 1500);
                }
            }
        });

        setContentView(R.layout.main_activity);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(R.id.container, new PlaceholderFragment()).commit();
        }
    }

    public static class PlaceholderFragment extends Fragment {
        private GLSurfaceView mGLSurfaceView = null;
        private RobotEar mEar;
        private RobotBody mBody;
        private RobotBrain mBrain;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.main_fragment, container, false);

            final Context context = getActivity();

            mEar = new RobotEar(context);
            mBody = new RobotBody(context);
            mBrain = new RobotBrain(context, mBody, mEar);

            mGLSurfaceView = (GLSurfaceView)rootView.findViewById(R.id.gl_surface_view);
            mGLSurfaceView.setRenderer(mBody.getRenderer());

            rootView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        mBody.blink();
                        mBody.sp();// TESTTEST
                        mEar.relaunch();
                        getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
                        return true;
                    }
                    return false;
                }
            });

            return rootView;
        }

        @Override
        public void onResume() {
            super.onResume();

            mEar.onResume();
            mBody.onResume();
            mBrain.onResume();
        }

        @Override
        public void onPause() {
            mBrain.onPause();
            mBody.onPause();
            mEar.onPause();

            super.onPause();
        }
    }
}
