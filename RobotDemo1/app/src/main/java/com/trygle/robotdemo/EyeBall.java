package com.trygle.robotdemo;

import java.nio.FloatBuffer;
import java.util.ArrayList;

import javax.microedition.khronos.opengles.GL10;

public class EyeBall extends GLObject {
    private final ArrayList<FloatBuffer> mVertexBuffer = new ArrayList<FloatBuffer>();
    private final ArrayList<FloatBuffer> mNormalBuffer = new ArrayList<FloatBuffer>();
    private final int mDepth;
    private final int mTotalNumStrips;

    public EyeBall(final int depth) {
        mDepth = Math.max(1, Math.min(5, depth));
        mTotalNumStrips = (int)Math.pow(2, mDepth - 1) * 5;
        createModel(1, 1);
    }

    public void setParam(final float radius, final float yradius) {
        createModel(radius, yradius);
    }

    private void createModel(final float radius, final float yradius) {
        final int numVerticesPerStrip = (int)Math.pow(2, mDepth) * 3;
        final double altitudeStepAngle = 2 * Math.PI / 3 / Math.pow(2, mDepth);
        final double azimuthStepAngle = 2 * Math.PI / this.mTotalNumStrips;
        double x, y, z, h, altitude, azimuth;

        for (int stripNum = 0; stripNum < mTotalNumStrips; stripNum++) {
            final float[] vertices = new float[numVerticesPerStrip * 3];
            final float[] normals = new float[numVerticesPerStrip * 3];
            int vertexPos = 0;
            int normalPos = 0;

            altitude = Math.PI / 2;
            azimuth = stripNum * azimuthStepAngle;
            for (int vertexNum = 0; vertexNum < numVerticesPerStrip; vertexNum += 2) {
                y = Math.sin(altitude);
                h = Math.cos(altitude);
                z = h * Math.sin(azimuth);
                x = h * Math.cos(azimuth);
                vertices[vertexPos++] = (float) x * radius;
                vertices[vertexPos++] = (float) y * radius;
                vertices[vertexPos++] = (float) z * radius;
                normals[normalPos++] = (float) x;
                normals[normalPos++] = (float) y;
                normals[normalPos++] = (float) z;

                altitude -= altitudeStepAngle;
                azimuth -= azimuthStepAngle / 2.0;

                y = Math.sin(altitude);
                h = Math.cos(altitude);
                z = h * Math.sin(azimuth);
                x = h * Math.cos(azimuth);
                vertices[vertexPos++] = (float) x * radius;
                vertices[vertexPos++] = (float) y * radius;
                vertices[vertexPos++] = (float) z * radius;
                normals[normalPos++] = (float) x;
                normals[normalPos++] = (float) y;
                normals[normalPos++] = (float) z;

                azimuth += azimuthStepAngle;
            }

            addFloatBuffer(mVertexBuffer, vertices);
            addFloatBuffer(mNormalBuffer, normals);
        }
    }

    public void draw(final GL10 gl) {
        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
        gl.glEnableClientState(GL10.GL_NORMAL_ARRAY);

        gl.glPushMatrix();
        applyTransformation(gl);

        float[] specular = { 1.0f, 1.0f, 1.0f, 1.0f };
        gl.glMaterialfv(GL10.GL_FRONT_AND_BACK, GL10.GL_SPECULAR, specular, 0);
        gl.glMaterialf(GL10.GL_FRONT_AND_BACK, GL10.GL_SHININESS, 12f);
        gl.glFrontFace(GL10.GL_CW);
        for (int i = 0; i < mTotalNumStrips; i++) {
            gl.glVertexPointer(3, GL10.GL_FLOAT, 0, mVertexBuffer.get(i));
            gl.glNormalPointer(GL10.GL_FLOAT, 0, mNormalBuffer.get(i));
            gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, mVertexBuffer.get(i).capacity() / 3);
        }

        gl.glPopMatrix();

        gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
        gl.glDisableClientState(GL10.GL_NORMAL_ARRAY);
    }
}
