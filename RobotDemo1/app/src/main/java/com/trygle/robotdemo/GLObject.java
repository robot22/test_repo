package com.trygle.robotdemo;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.List;

import javax.microedition.khronos.opengles.GL10;

public class GLObject {
    private float mTranslateX = 0;
    private float mTranslateY = 0;
    private float mTranslateZ = 0;
    private float mScaleX = 1;
    private float mScaleY = 1;
    private float mScaleZ = 1;
    private float mRotationX = 0;
    private float mRotationY = 0;
    private float mRotationZ = 0;

    public void setTranslateX(float translateX) {
        mTranslateX = translateX;
    }

    public void setTranslateY(float translateY) {
        mTranslateY = translateY;
    }

    public void setTranslateZ(float translateZ) {
        mTranslateZ = translateZ;
    }

    public float getTranslateX() {
        return mTranslateX;
    }

    public float getTranslateY() {
        return mTranslateY;
    }

    public float getTranslateZ() {
        return mTranslateZ;
    }

    public void setScaleX(float scaleX) {
        mScaleX = scaleX;
    }

    public void setScaleY(float scaleY) {
        mScaleY = scaleY;
    }

    public void setScaleZ(float scaleZ) {
        mScaleZ = scaleZ;
    }

    public float getScaleX() {
        return mScaleX;
    }

    public float getScaleY() {
        return mScaleY;
    }

    public float getScaleZ() {
        return mScaleZ;
    }

    public void setRotationX(float RotationX) {
        mRotationX = RotationX;
    }

    public void setRotationY(float RotationY) {
        mRotationY = RotationY;
    }

    public void setRotationZ(float RotationZ) {
        mRotationZ = RotationZ;
    }

    public float getRotationX() {
        return mRotationX;
    }

    public float getRotationY() {
        return mRotationY;
    }

    public float getRotationZ() {
        return mRotationZ;
    }

    float r = 0;
    protected void applyTransformation(GL10 gl) {
        gl.glRotatef(mRotationX, 1, 0, 0);
        gl.glRotatef(mRotationY, 0, 1, 0);
        gl.glRotatef(mRotationZ, 0, 0, 1);
        gl.glTranslatef(mTranslateX, mTranslateY, mTranslateZ);
        gl.glScalef(mScaleX, mScaleY, mScaleZ);
    }

    protected void addFloatBuffer(List<FloatBuffer> floatBuffer, float[] floats) {
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(floats.length * Float.SIZE / 8);
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer fb = byteBuffer.asFloatBuffer();
        fb.put(floats);
        fb.position(0);
        floatBuffer.add(fb);
    }

    public void initialize(GL10 gl) {
    }

    public void draw(GL10 gl) {
    }
}
