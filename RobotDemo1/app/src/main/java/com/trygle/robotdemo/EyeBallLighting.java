package com.trygle.robotdemo;

import javax.microedition.khronos.opengles.GL10;

public class EyeBallLighting extends GLObject {
    private float mLighting;

    public void setLighting(float lighting) {
        mLighting = lighting;
    }

    public float getLighting() {
        return mLighting;
    }

    public void draw(GL10 gl) {
        float[] position0 = { -10.0f, 10.0f, 40.0f, 0.0f };
        float[] position1 = {   0.0f, 40.0f, -10.0f, 0.0f };
        float[] specular0 = { 1.0f, 1.0f, 1.0f, 1.0f };
        float[] specular1 = { 0.0f, 0.0f, 0.0f, 1.0f };
        float[] diffuse0 = { 0.8f, 0.8f, 0.8f, 1.0f };
        float[] diffuse1 = { 10.0f, 10.0f, 10.0f, 1.0f };
        float[] ambient0 = { 0.8f, 0.8f, 0.8f, 1.0f };
        float[] ambient1 = { 0.0f, 0.0f, 0.0f, 1.0f };
        float[] position = new float[4];
        float[] specular = new float[4];
        float[] diffuse = new float[4];
        float[] ambient = new float[4];
        for (int i = 0; i < 4; i++) {
            position[i] = position0[i] * (1f - mLighting) + position1[i] * mLighting;
            specular[i] = specular0[i] * (1f - mLighting) + specular1[i] * mLighting;
            diffuse[i] = diffuse0[i] * (1f - mLighting) + diffuse1[i] * mLighting;
            ambient[i] = ambient0[i] * (1f - mLighting) + ambient1[i] * mLighting;
        }
        gl.glLightfv(GL10.GL_LIGHT0, GL10.GL_POSITION, position, 0);
        gl.glLightfv(GL10.GL_LIGHT0, GL10.GL_SPECULAR, specular, 0);
        gl.glLightfv(GL10.GL_LIGHT0, GL10.GL_DIFFUSE, diffuse, 0);
        gl.glLightfv(GL10.GL_LIGHT0, GL10.GL_AMBIENT, ambient, 0);
    }
}
