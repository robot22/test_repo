package com.trygle.robotdemo;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.opengl.GLUtils;
import android.util.Log;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

/**
 * Created by demo on 2015/07/17.
 */
public class Animation2D extends GLObject {

    // コンテキスト
    private Context mContext;

    // 設定ファイル名
    private String settingsName;
    private String imageName;

    private Bitmap mBitmap;
    private Canvas mCanvas;
    private Paint mPaint;

    // 座標
    private int imagePosY[];// アニメーションの数だけ用意

    // サイズ   アニメーションの数だけ用意
    private int bmpWidth[];
    private int bmpHeight[];

    // フレーム位置
    private int framePosNow;// 現在
    private int framePosMax[];// 最終:アニメーションの数だけ用意

    // アニメーションID
    // 0が先頭、1以降は縦の数
    private int animeId = 0;

    // アニメーションフラグ
    private boolean isAnimation = false;

    private boolean isInit = false;


    // アニメーションの情報///////////////

    //////////////////////////////////////



    protected final int[] mTextures = new int[1];

    private float textureVert[];
    private FloatBuffer textureVertBuff;

    private float textureUV[];
    private FloatBuffer textureUvBuff;

    // メンバ関数----------------------------------------------------------

    public Animation2D( Context context, String settingsName )
    {
        this.mContext = context;
        this.settingsName = settingsName;

    }

    public void initialize(final GL10 gl) {
        gl.glGenTextures(1, mTextures, 0);

        isAnimation = false;
    }

    public boolean LoadAnimation()
    {
        boolean result = false;

        // ファイルからアニメーションの情報を取得する。
        // アニメーションの情報を取得したら画像を読み込む

        if ((this.isInit = LoadSettings())) {
            // 画像読込
            Resources resources = this.mContext.getResources();
            R.drawable rDrawable = new R.drawable();

            try {
                Field field = rDrawable.getClass().getField(this.imageName);
                int resId = field.getInt(rDrawable);
                this.mBitmap = BitmapFactory.decodeResource(resources, resId);

                result = true;
            }
            catch(Exception ex)
            {
                Log.e("Exceptoion", ex.getMessage());
            }
        }

        return result;
    }

    // 設定読込
    // ここで各設定値を読み込み、変数に格納する。
    private boolean LoadSettings()
    {
        boolean result = false;
        try {
            XmlPullParserFactory factory;
            factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();


            AssetManager asset = mContext.getResources().getAssets();

            // XMLファイルのストリーム情報を取得
            InputStream is = null;
            is = asset.open(settingsName);
            InputStreamReader isr = new InputStreamReader(is);
            xpp.setInput(isr);

            int eventType = xpp.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_DOCUMENT) {
                    System.out.println("Start document");
                } else if (eventType == XmlPullParser.START_TAG) {
                    System.out.println("Start tag " + xpp.getName());
                } else if (eventType == XmlPullParser.END_TAG) {
                    System.out.println("End tag " + xpp.getName());
                } else if (eventType == XmlPullParser.TEXT) {
                    System.out.println("Text " + xpp.getText());
                }
                eventType = xpp.next();
            }

            result = true;
        } catch (Exception ex)
        {
            Log.e("Exception", ex.getMessage());// そのまま
        }

        return result;
    }

    private void createModel(final float width) {
        textureVertBuff = makeFloatBuffer(textureVert);
        textureUvBuff = makeFloatBuffer(textureUV);
    }


    public void draw(final GL10 gl) {
        // 透明色で塗りつぶす
        mCanvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        int textureID = mTextures[0];
        gl.glBindTexture(GL10.GL_TEXTURE_2D, textureID);


        if( isAnimation ) {

            gl.glGenTextures(1, mTextures, 0);




            // フレーム更新
            this.framePosNow++;
            if( this.framePosNow > this.framePosMax[this.animeId])
            {
                this.framePosNow = 0;
                isAnimation = false;
            }
        }

        GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, mBitmap, 0);
        gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_LINEAR);
        gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);

        // 2Dテクスチャ有効化
        gl.glEnable(GL10.GL_TEXTURE_2D);

        gl.glBindTexture(GL10.GL_TEXTURE_2D, textureID);

        // 頂点バッファ
        gl.glVertexPointer(2, GL10.GL_FLOAT, 0, textureVertBuff);
        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);

        // UVバッファ
        gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, textureUvBuff);
        gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);

        // 描画
        gl.glPushMatrix();
        gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, 4);
        gl.glPopMatrix();

        // 無効化
        gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
        gl.glDisable(GL10.GL_TEXTURE_2D);

    }


    public void StartAnimation(int animeId)
    {
        synchronized (this)
        {
            this.isAnimation = true;
            this.animeId = animeId;
            this.framePosNow = 0;
        }
    }

    public void StopAnimation()
    {
        synchronized (this)
        {
            isAnimation = false;
        }
    }



    private static FloatBuffer makeFloatBuffer(float[] values) {
        ByteBuffer bb = ByteBuffer.allocateDirect(values.length*4);
        bb.order(ByteOrder.nativeOrder());
        FloatBuffer fb = bb.asFloatBuffer();
        fb.put(values);
        fb.position(0);
        return fb;
    }

}
