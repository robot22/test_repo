package com.trygle.robotdemo;

import java.nio.FloatBuffer;
import java.util.ArrayList;

import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLUtils;

public class TexturePlain extends GLObject {
    private final Context mContext;
    private final int mResId;

    private final ArrayList<FloatBuffer> mVertexBuffer = new ArrayList<FloatBuffer>();
    private final ArrayList<FloatBuffer> mNormalBuffer = new ArrayList<FloatBuffer>();
    private final ArrayList<FloatBuffer> mTextureBuffer = new ArrayList<FloatBuffer>();
    private final int[] mTextures = new int[1];

    private float mAlpha;

    public TexturePlain(Context context, int resId) {
        mContext = context;
        mResId = resId;

        createModel(1.0f);
    }

    public void setAlpha(float alpha) {
        mAlpha = alpha;
    }

    public float getAlpha() {
        return mAlpha;
    }

    private void createModel(final float width) {
        final float[] vertices = new float[4 * 3];
        final float[] normals = new float[4 * 3];
        final float[] texturePoints = new float[4 * 2];
        int vertexPos = 0;
        int normalPos = 0;
        int texturePos = 0;
        for (int i = 0; i < 4; i++) {
            float x = i % 2;
            float y = i / 2;
            vertices[vertexPos++] = x * 2 - 1;
            vertices[vertexPos++] = y * 2 - 1;
            vertices[vertexPos++] = 0;
            normals[normalPos++] = 0;
            normals[normalPos++] = 0;
            normals[normalPos++] = -1;
            texturePoints[texturePos++] = x;
            texturePoints[texturePos++] = 1 - y;
        }

        addFloatBuffer(mVertexBuffer, vertices);
        addFloatBuffer(mNormalBuffer, normals);
        addFloatBuffer(mTextureBuffer, texturePoints);
    }

    public void loadGLTexture(final GL10 gl, final Context context, final int texture) {
        final Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), texture);

        gl.glGenTextures(1, this.mTextures, 0);
        gl.glBindTexture(GL10.GL_TEXTURE_2D, this.mTextures[0]);

        gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_NEAREST);
        gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);

        GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bitmap, 0);

        bitmap.recycle();
    }

    public void initialize(final GL10 gl) {
        loadGLTexture(gl, mContext, mResId);
    }

    public void draw(final GL10 gl) {
        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
        //gl.glEnableClientState(GL10.GL_COLOR_ARRAY);
        //gl.glEnableClientState(GL10.GL_NORMAL_ARRAY);
        gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);

        gl.glEnable(GL10.GL_TEXTURE_2D);
        gl.glShadeModel(GL10.GL_SMOOTH);
        gl.glBindTexture(GL10.GL_TEXTURE_2D, mTextures[0]);

        gl.glPushMatrix();
        applyTransformation(gl);

        // gl.glTexEnvx(GL10.GL_TEXTURE_ENV, GL10.GL_TEXTURE_ENV_MODE, GL10.GL_MODULATE);
        // gl.glColor4f(1.0f, 0.5f, 1.0f, mAlpha);

        for (int i = 0; i < mVertexBuffer.size(); i++) {
            gl.glVertexPointer(3, GL10.GL_FLOAT, 0, mVertexBuffer.get(i));
            //glNormalPointer(GL10.GL_FLOAT, 0, mNormalBuffer.get(i));
            gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, mTextureBuffer.get(i));
            gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, mVertexBuffer.get(i).capacity() / 3);
        }

        gl.glPopMatrix();

        gl.glDisable(GL10.GL_TEXTURE_2D);

        gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
        gl.glDisableClientState(GL10.GL_COLOR_ARRAY);
        //gl.glDisableClientState(GL10.GL_NORMAL_ARRAY);
        gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
    }
}
