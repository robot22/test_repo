package com.trygle.robotdemo;

import java.util.ArrayList;
import java.util.List;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;
import android.widget.Toast;

public class RobotEar {
    public interface Listener {
        public void onResult(List<String> list);
        public void onBeginning();
        public void onInterval();
    }

    private static final String TAG = "RobotEar";
    private Context mContext;
    private Handler mHandler;
    private Listener mListener = null;
    private SpeechRecognizer mSpeechRecognizer = null;

    public RobotEar(Context context) {
        mContext = context;
        mHandler = new Handler();
        mListener = null;
        mSpeechRecognizer = null;
    }

    private void createSpeechRecognizer() {
        if (mSpeechRecognizer != null) {
            return;
        }

        mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(mContext);
        mSpeechRecognizer.setRecognitionListener(new RecognitionListener() {
            @Override
            public void onReadyForSpeech(Bundle params) {
                Log.d(TAG, "onReadyForSpeech");
            }
            @Override
            public void onBeginningOfSpeech() {
                Log.d(TAG, "onBeginningOfSpeech");
                mListener.onBeginning();
            }
            @Override
            public void onRmsChanged(float rmsdB) {
                /*Log.d(TAG, "onRmsChanged");*/
            }
            @Override
            public void onBufferReceived(byte[] buffer) {
                Log.d(TAG, "onBufferReceived");
            }
            @Override
            public void onEndOfSpeech() {
                Log.d(TAG, "onEndOfSpeech");
            }

            @Override
            public void onResults(Bundle results) {
                Log.d(TAG, "onResults");
                ArrayList<String> recData = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                mListener.onResult(recData);
                launchSpeechRecognizer(0);
            }

            @Override
            public void onError(int error) {
                Log.d(TAG, "onError " + error);
                switch (error) {
                case SpeechRecognizer.ERROR_AUDIO:
                    Toast.makeText(mContext, "音声データの保存に失敗しました", Toast.LENGTH_LONG).show();
                    break;
                case SpeechRecognizer.ERROR_CLIENT:
                    Toast.makeText(mContext, "端末内でエラーが発生しました", Toast.LENGTH_LONG).show();
                    break;
                case SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS:
                    Toast.makeText(mContext, "録音権限がありません", Toast.LENGTH_LONG).show();
                    break;
                case SpeechRecognizer.ERROR_NETWORK:
                    Toast.makeText(mContext, "ネットワークのエラーです", Toast.LENGTH_LONG).show();
                    break;
                case SpeechRecognizer.ERROR_NETWORK_TIMEOUT:
                    Toast.makeText(mContext, "ネットワークがタイムアウトしました", Toast.LENGTH_LONG).show();
                    break;
                case SpeechRecognizer.ERROR_NO_MATCH:
                    mListener.onInterval();
                    launchSpeechRecognizer(0);
                    break;
                case SpeechRecognizer.ERROR_RECOGNIZER_BUSY:
                    Toast.makeText(mContext, "認識サーバにつながりませんでした", Toast.LENGTH_LONG).show();
                    break;
                case SpeechRecognizer.ERROR_SERVER:
                    Toast.makeText(mContext, "認識サーバのエラーです", Toast.LENGTH_LONG).show();
                    launchSpeechRecognizer(1000);
                    break;
                case SpeechRecognizer.ERROR_SPEECH_TIMEOUT:
                    mListener.onInterval();
                    launchSpeechRecognizer(0);
                    break;
                default:
                    Toast.makeText(mContext, "原因不明のエラーです", Toast.LENGTH_LONG).show();
                    break;
                }
            }

            @Override
            public void onPartialResults(Bundle partialResults) {
                Log.d(TAG, "onPartialResults");
            }

            @Override
            public void onEvent(int eventType, Bundle params) {
                Log.d(TAG, "onEvent " + eventType);
            }
        });
    }

    private void destroySpeechRecognizer() {
        if (mSpeechRecognizer != null) {
            mSpeechRecognizer.destroy();
        }
        mSpeechRecognizer = null;
    }

    public void onResume() {
        launchSpeechRecognizer(500);
    }

    public void onPause() {
        stopSpeechRecognizer();
    }

    public void relaunch() {
        launchSpeechRecognizer(10);
    }

    public void setListener(Listener listener) {
        mListener = listener;
    }

    private void launchSpeechRecognizer(int msec) {
        Log.d(TAG, "launchSpeechRecognizer");
        destroySpeechRecognizer();
        createSpeechRecognizer();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                    intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                    intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "何か御用ですか");
                    intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, mContext.getPackageName());
                    Log.d(TAG, "startListening");
                    mSpeechRecognizer.startListening(intent);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(mContext, "Speech Recognizer Activity Not Found", Toast.LENGTH_LONG).show();
                }
            }
        }, msec);
    }

    private void stopSpeechRecognizer() {
        Log.d(TAG, "stopSpeechRecognizer");
        mSpeechRecognizer.cancel();
    }
}
