package com.trygle.robotdemo;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.ftdi.j2xx.D2xxManager;
import com.ftdi.j2xx.D2xxManager.D2xxException;
import com.ftdi.j2xx.FT_Device;

public class ServoController {
    private Context mContext;
    private D2xxManager ftdid2xx;
    private FT_Device mFTDevice;

    private int mBaudRate;
    private int mDeviceCount;
    private int mCurrentDeviceIndex;

    public boolean mReadThreadGoing = false;
    public ReadThread mReadThread;

    public ServoController(Context context) throws D2xxException {
        mContext = context;
        ftdid2xx = D2xxManager.getInstance(context);
        ftdid2xx.setVIDPID(5724, 9);

        mBaudRate = 1500000;
        mDeviceCount = -1;
        mCurrentDeviceIndex = -1;
    }

    public void onResume() {
        mDeviceCount = 0;
        createDeviceList();
        if (mDeviceCount > 0) {
            connectFunction(0);
        } else {
            Toast.makeText(mContext, "no device attached", Toast.LENGTH_SHORT).show();
        }
    }

    public void onPause() {
        disconnectFunction();
    }

    public void notifyUSBDeviceAttach() {
        createDeviceList();
    }

    public void notifyUSBDeviceDetach() {
        disconnectFunction();
    }	

    public void createDeviceList() {
        int tempDevCount = ftdid2xx.createDeviceInfoList(mContext);

        if (tempDevCount > 0) {
            if (mDeviceCount != tempDevCount) {
                mDeviceCount = tempDevCount;
                // Toast.makeText(mContext, mDeviceCount + " port device attached", Toast.LENGTH_SHORT).show();
            }
        } else {
            mDeviceCount = -1;
            mCurrentDeviceIndex = -1;
        }
    }

    public void connectFunction(int openIndex) {
        int readablePortNumber = openIndex + 1;
        if (mCurrentDeviceIndex != openIndex) {
            if (mFTDevice == null) {
                mFTDevice = ftdid2xx.openByIndex(mContext, openIndex);
            } else {
                synchronized(mFTDevice) {
                    mFTDevice = ftdid2xx.openByIndex(mContext, openIndex);
                }
            }
        } else {
            Toast.makeText(mContext,"Device port " + readablePortNumber + " is already opened",Toast.LENGTH_LONG).show();
            return;
        }
        if (mFTDevice == null) {
            Toast.makeText(mContext,"open device port(" + readablePortNumber + ") NG, ftDev == null", Toast.LENGTH_LONG).show();
            return;
        }

        if (mFTDevice.isOpen()) {
            mCurrentDeviceIndex = openIndex;

            if (!mReadThreadGoing) {
                mReadThread = new ReadThread();
                mReadThread.start();
                mReadThreadGoing = true;
            }

            // setConfig
            mFTDevice.setBitMode((byte) 0, D2xxManager.FT_BITMODE_RESET);
            mFTDevice.setBaudRate(mBaudRate);
            mFTDevice.setDataCharacteristics(D2xxManager.FT_DATA_BITS_8, D2xxManager.FT_STOP_BITS_1, D2xxManager.FT_PARITY_NONE);
            mFTDevice.setFlowControl(D2xxManager.FT_FLOW_NONE, (byte)0x0b, (byte)0x0d);

            Toast.makeText(mContext, "open device port(" + readablePortNumber + ") OK", Toast.LENGTH_SHORT).show();
            // Toast.makeText(mContext, "Config done", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(mContext, "open device port(" + readablePortNumber + ") NG", Toast.LENGTH_LONG).show();
        }
    }

    public void disconnectFunction() {
        mDeviceCount = -1;
        mCurrentDeviceIndex = -1;
        mReadThreadGoing = false;

        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (mFTDevice != null) {
            synchronized(mFTDevice) {
                if (mFTDevice.isOpen()) {
                    mFTDevice.close();
                }
            }
        }
    }

    private class ReadThread extends Thread {
        public ReadThread() {
            this.setPriority(Thread.MIN_PRIORITY);
        }

        @Override
        public void run() {
            final int readLength = 512;
            byte[] readData = new byte[readLength];

            while (mReadThreadGoing) {
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    mReadThreadGoing = false;
                    break;
                }
                synchronized (mFTDevice) {
                    int available = mFTDevice.getQueueStatus();
                    if (available > 0) {
                        if (available > readLength){
                            available = readLength;
                        }
                        mFTDevice.read(readData, available);
                    }
                }
            }
        }
    }

    public void sendMessage(byte[] data) {
        if (mFTDevice == null || !mFTDevice.isOpen()) {
            Log.e("j2xx", "SendMessage: device not open");
            return;
        }
        mFTDevice.setLatencyTimer((byte)16);
        mFTDevice.write(data, data.length);
        // Toast.makeText(mContext, String.format("%02x %02x %02x %02x %02x %02x %02x %02x %02x", data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7], data[8]), Toast.LENGTH_SHORT).show();
    }

    private void setCheckSum(byte[] data) {
        int s = 0;
        for (int i = 0; i < data.length - 1; i++) {
            s += data[i];
        }
        data[data.length - 1] = (byte)(s & 0xff);
    }

    public void initialize(int servo) {
        byte[] data = new byte[] {0x09, 0x04, 0x00,    0, 0x00, 0x01, 0x28, 0x01,    0}; 
        data[3] = (byte)servo;
        setCheckSum(data);
        sendMessage(data);
    }

    public void setTrajectory(int servo, int interpolation) {
        byte[] data = new byte[] {0x09, 0x04, 0x00,    0, 0x00,    0, 0x29, 0x01,    0}; 
        data[3] = (byte)servo;
        data[5] = (byte)interpolation;
        setCheckSum(data);
        sendMessage(data);
    }

    public void setPosition(int servo, int degree) {
        byte[] data = new byte[] {0x09, 0x04, 0x00,    0,    0,    0, 0x2a, 0x01,    0};
        data[3] = (byte)servo;
        data[4] = (byte)(degree & 0xff);
        data[5] = (byte)((degree >> 8) & 0xff);
        setCheckSum(data);
        sendMessage(data);
    }

    public void setSpeed(int servo, int speed) {
        byte[] data = new byte[] {0x09, 0x04, 0x00,    0,    0,    0, 0x30, 0x01,    0};
        data[3] = (byte)servo;
        data[4] = (byte)(speed & 0xff);
        data[5] = (byte)((speed >> 8) & 0xff);
        setCheckSum(data);
        sendMessage(data);
    }

    public void setDuration(int servo, int duration) {
        byte[] data = new byte[] {0x09, 0x04, 0x00,    0,    0,    0, 0x36, 0x01,    0};
        data[3] = (byte)servo;
        data[4] = (byte)(duration & 0xff);
        data[5] = (byte)((duration >> 8) & 0xff);
        setCheckSum(data);
        sendMessage(data);
    }

    public void setPositions(int deg1, int deg2, int duration) {
        byte[] data = new byte[] {0x0c, 0x06, 0x00, 0x01,    0,    0, 0x02,    0,    0,    0,    0,    0};
        data[4] = (byte)(deg1 & 0xff);
        data[5] = (byte)((deg1 >> 8) & 0xff);
        data[7] = (byte)(deg2 & 0xff);
        data[8] = (byte)((deg2 >> 8) & 0xff);
        data[9] = (byte)(duration & 0xff);
        data[10] = (byte)((duration >> 8) & 0xff);
        setCheckSum(data);
        sendMessage(data);
    }

}
