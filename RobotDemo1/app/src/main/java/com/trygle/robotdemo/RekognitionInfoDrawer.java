package com.trygle.robotdemo;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.SurfaceTexture;
import android.opengl.GLUtils;
import android.util.Log;

import com.panasonic.three.RekoSDK;

import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

import javax.microedition.khronos.opengles.GL10;

/**
 * Created by demo on 2015/07/14.
 */
public class RekognitionInfoDrawer extends GLObject {

    private int textureId = -1;
    protected final int[] mTextures = new int[1];
    protected SurfaceTexture mSurfaceTexture;

    private final ArrayList<FloatBuffer> mVertexBuffer = new ArrayList<FloatBuffer>();
    private final ArrayList<FloatBuffer> mNormalBuffer = new ArrayList<FloatBuffer>();
    private final ArrayList<FloatBuffer> mTextureBuffer = new ArrayList<FloatBuffer>();

    private Bitmap mBitmap;
    private Canvas mCanvas;
    private Paint mPaint;

    private int bmpWidth;
    private int bmpHeight;

    // 表示タイトル
    private String[] rekoTitles = { "年齢", "笑顔", "美顔度", "性別", "誰に似ているか", "似ている度合" };

    // 文字サイズ
    private final float TEXT_SIZE = 10.0f;

    // 文字色
    private final int TEXT_COLOR = Color.WHITE;

    // 線の幅
    private final float STROKE_WIDTH = 2f;

    //
    private float posX = 15.0f;
    private float posY = 15.0f;//

    private float betweenX = 70.0f;
    private float betweenY = 15.0f;

    private StringBuffer text = new StringBuffer(32);

    // 画面に表示するRekognitionの情報
    enum RekognitionItem
    {
        Age,
        Smile,
        Beauty,
        Sex,
        SimilarName,
        SimilarPercentage,
    }
    private double age = 0;
    private int smile = 0;
    private double beauty = 0;
    private int sex = 0;
    private String similarName = "";
    private double similarPercentage = 0;

    // 心拍グラフ
    private float heatBeatGraphPosX = 10.0f;
    private float heatBeatGraphPosY = 120.0f;
    private float heatBeatGraphWidth = 120.0f;
    private float heatBeatGraphHeight = 120.0f;

    private Queue<Integer> heatBeatQueue;
    private float heatBeatScrollPosX = 0.0f;
    private float heatBeatScrollSpeedX = 1.0f;// スクロール速度
    private float heatBeatInterval = 10.0f;// 折れ線グラフの感覚


    private float textureVert[];
    private FloatBuffer textureVertBuff;

    private float textureUV[];
    private FloatBuffer textureUvBuff;

    // 描画フラグ
    private boolean isDraw = true;

    public RekognitionInfoDrawer(Context context)
    {
        this.textureVert = new float[] {
            1.0f,  -1.0f,
            2.0f,  -1.0f,
            1.0f,  1.0f,
            2.0f,  1.0f,
        };

        this.textureUV = new float[] {
                0.0f, 1.0f,
                1.0f, 1.0f,
                0.0f, 0.0f,
                1.0f, 0.0f,
        };

        createModel(1.0f);

        this.heatBeatQueue = new LinkedList<Integer>();
        // TESTTEST
        // データが取れるようになれば削除
        for( int i=0; i<10; i++ )
        {
            this.heatBeatQueue.add((int) (Math.random() * 100));
        }

    }

    private void createModel(final float width) {
        textureVertBuff = makeFloatBuffer(textureVert);
        textureUvBuff = makeFloatBuffer(textureUV);
    }

    public void initialize(final GL10 gl) {
        gl.glGenTextures(1, mTextures, 0);

        mBitmap = Bitmap.createBitmap(128,256, Bitmap.Config.ARGB_8888);//�T�C�Y�͉�

        mCanvas = new Canvas(mBitmap);

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setColor(Color.WHITE);
        mPaint.setTextSize(TEXT_SIZE);
        mPaint.setStrokeWidth(STROKE_WIDTH);

        isDraw = true;
    }

    public void switchPreview()
    {
        synchronized (this) {
            isDraw = !isDraw;
        }
    }

    public void startPreview() {
        synchronized (this) {
            isDraw = true;
        }
    }
    public void stopPreview() {
        synchronized (this) {
            isDraw = false;
        }
    }

    //
    private String GetValue(int id)
    {
        String result = "";
        RekognitionItem[] values = RekognitionItem.values();
        RekognitionItem item = values[id];

        switch (item)
        {
            case Age:
                result = Double.toString(this.age);
                break;

            case Smile:
                result = (this.smile == 0 ) ? "笑顔ではない" : "笑顔";
                break;

            case Beauty:
                result = Double.toString(this.beauty);
                break;

            case Sex:
                result = (this.sex == 0 ) ? "女性" : "男性";
                break;

            default:
                result = "-1";
                break;
        }

        return result;
    }

    public void draw(final GL10 gl) {

        if (textureId != 0) {
            gl.glDeleteTextures(1, mTextures, 0);
        }

        gl.glGenTextures(1, mTextures, 0);
        textureId = mTextures[0];
        gl.glBindTexture(GL10.GL_TEXTURE_2D, textureId);

        // 透明色で塗りつぶす
        mCanvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);

        if( isDraw ) {
            Object rekoData = RekoSDK.GetRekoData();
            if( rekoData != null ) {
                this.SetRekognitionData((JSONObject)rekoData);
            }
            // TESTTEST
            /*
            this.age = (int) (Math.random() * 100);
            this.smile = (int) (Math.random() * 100);
            this.beauty = (int) (Math.random() * 100);
            this.sex = (int) (Math.random() * 100);
            */
            // TESTTEST

            // 文字
            int itemNum = rekoTitles.length;
            for (int i = 0; i < itemNum; i++) {
                mCanvas.drawText(rekoTitles[i], posX, posY + (betweenY * i), mPaint);
                mCanvas.drawText(":", posX + betweenX, posY + (betweenY * i), mPaint);
                mCanvas.drawText(GetValue(i), posX + betweenX + 15, posY + (betweenY * i), mPaint);
            }

            // 心拍グラフ
            heatBeatScrollPosX += heatBeatScrollSpeedX;
            if (heatBeatScrollPosX >= heatBeatGraphWidth) {
                heatBeatScrollPosX -= heatBeatInterval;
                // TESTTEST
                // 値が取得できるようになれば削除
                this.heatBeatQueue.remove();
                this.heatBeatQueue.add((int) (Math.random() * 100));
                // TESTTEST
            }

            Integer heatBeatList[] = this.heatBeatQueue.toArray(new Integer[0]);
            int heatBeatNum = heatBeatList.length;

            for (int i = 0; i < heatBeatNum - 1; i++) {
                int heatBeat1 = heatBeatList[i];
                int heatBeat2 = heatBeatList[i + 1];
                mCanvas.drawLine(this.heatBeatScrollPosX - (this.heatBeatInterval * i),
                        heatBeatGraphPosY + heatBeatGraphHeight - heatBeat1,
                        this.heatBeatScrollPosX - (this.heatBeatInterval * (i + 1)),
                        heatBeatGraphPosY + heatBeatGraphHeight - heatBeat2,
                        this.mPaint);
            }
        }

        GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, mBitmap, 0);
        gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_LINEAR);
        gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);

        // 2Dテクスチャ有効化
        gl.glEnable(GL10.GL_TEXTURE_2D);

        gl.glBindTexture(GL10.GL_TEXTURE_2D, textureId);

        // 頂点バッファ
        gl.glVertexPointer(2, GL10.GL_FLOAT, 0, textureVertBuff);
        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);

        // UVバッファ
        gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, textureUvBuff);
        gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);

        // 描画
        gl.glPushMatrix();
        gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, 4);
        gl.glPopMatrix();

        // 無効化
        gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
        gl.glDisable(GL10.GL_TEXTURE_2D);
    }

    private static FloatBuffer makeFloatBuffer(float[] values) {
        ByteBuffer bb = ByteBuffer.allocateDirect(values.length*4);
        bb.order(ByteOrder.nativeOrder());
        FloatBuffer fb = bb.asFloatBuffer();
        fb.put(values);
        fb.position(0);
        return fb;
    }

    public void AddHeatBeat(int heatBeat)
    {
        this.heatBeatQueue.add(heatBeat);
    }

    /**
     * JSONObjectより年齢、美顔度、誰に似ているかの情報を取得する。
     * 似ている人物は一番スコアが高い人物の名前を表示する。(一番スコアが高い人物が先頭に入っている)
     * @param jsonObject
     */
    public void SetRekognitionData( JSONObject jsonObject)
    {
        if (jsonObject.has("face_detection"))
        {
            try {
                this.age = jsonObject.getDouble("age");
                this.smile = jsonObject.getInt("smile");
                this.beauty = jsonObject.getDouble("beauty");
                this.sex = jsonObject.getInt("sex");

                // 誰に似ているかを取得
                JSONArray jsonArray = jsonObject.getJSONArray("face_detection");

                        int similarNum = jsonArray.length();
                for(int i=0; i<similarNum; i++ )
                {
                    JSONObject json = jsonArray.getJSONObject(i);
                    if (!json.has("matches")) {
                        continue;
                    }
                    JSONArray jsonArrayMatches = json.getJSONArray("matches");

                    // 一番スコアが高い人物が先頭に入っている
                    JSONObject jsonMatches = jsonArrayMatches.getJSONObject(0);

                    this.similarPercentage = jsonMatches.getDouble("score");
                    this.similarName = jsonMatches.getString("tag");

                }
            }
            catch (Exception e)
            {
                Log.v("Exception", e.getMessage());
            }
        }
    }
}
