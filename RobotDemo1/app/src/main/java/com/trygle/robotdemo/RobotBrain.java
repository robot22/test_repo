package com.trygle.robotdemo;

import java.util.List;

import android.content.Context;
import android.widget.Toast;

public class RobotBrain {
    private Context mContext;
    private RobotBody mBody;
    private int mMode;

    RobotBrain(Context context, RobotBody body, RobotEar ear) {
        mContext = context;
        mBody = body;
        mMode = 0;

        ear.setListener(new RobotEar.Listener() {
            public void onResult(List<String> recData) {
                if (mMode == 0 && handleNormalAction(recData)) {
                    return;
                }
                if (mMode == 1 && handleCameraAction(recData)) {
                    return;
                }
                if (mMode == 2 && handleWatchAction(recData)) {
                    return;
                }
                handleDefaultAction(recData);
            }

            public void onBeginning() {
                mBody.nod();
            }
            
            public void onInterval() {
                mBody.blink();
            }
        });
    }

    public void onResume() {
    }

    public void onPause() {
    }

    private boolean handleDefaultAction(List<String> recData) {
        if (matchString(recData, "左") || matchString(recData, "ひだり")) {
            mBody.lookLeft();
            return true;
        }
        if (matchString(recData, "右") || matchString(recData, "みぎ")) {
            mBody.lookRight();
            return true;
        }
        if (matchString(recData, "後ろ") || matchString(recData, "うしろ") || matchString(recData, "あぶない") || matchString(recData, "危ない")) {
            mBody.dangerous();
            return true;
        }

        if (matchString(recData, "戻れ") || matchString(recData, "もどれ")) {
            mBody.reset();
            return true;
        }

        if (matchString(recData, "回れ") || matchString(recData, "まわれ")) {
            mBody.grungrun();
            return true;
        }

        String getData = new String();
        getData = "「" + recData.get(0) + "」ってなに？";
        mBody.dontknow();
        Toast.makeText(mContext, getData, Toast.LENGTH_SHORT).show();

        return true;
    }

    private boolean handleNormalAction(List<String> recData) {
        if (matchString(recData, "カメラ") || matchString(recData, "写真")) {
            mBody.cameraStandBy();
            mMode = 1;
            return true;
        }
        int sec = getSeconds(recData);
        if (sec >= 0) {
            mBody.startWatch(sec);
            mMode = 2;
            return true;
        }
        if (matchString(recData, "おはよう") || matchString(recData, "こんにちは") || matchString(recData, "こんばんは")) {
            mBody.cameraStandBy();
            //mBody.greetings();
            return true;
        }
        if (matchString(recData, "かっこいい") || matchString(recData, "かわいい") || matchString(recData, "すてき")) {
            mBody.smile();
            return true;
        }
        return false;
    }

    private boolean handleCameraAction(List<String> recData) {
        if (matchString(recData, "カメラ") || matchString(recData, "しまって") || matchString(recData, "もう") || matchString(recData, "いい")) {
            mBody.cameraOut();
            mMode = 0;
            return true;
        }
        if (matchString(recData, "チーズ")) {
            mBody.takePicture();
            return true;
        }
        int sec = getSeconds(recData);
        if (sec >= 0) {
            mBody.cameraOut();
            mBody.startWatch(sec);
            mMode = 2;
        }
        return false;
    }

    private boolean handleWatchAction(List<String> recData) {
        if (matchString(recData, "カメラ") || matchString(recData, "写真")) {
            mBody.stopWatch();
            mBody.cameraStandBy();
            mMode = 1;
            return true;
        }
        int sec = getSeconds(recData);
        if (sec >= 0) {
            mBody.resetWatch(sec);
            return true;
        }
        if (matchString(recData, "しまって") || matchString(recData, "もう") || matchString(recData, "いい")) {
            mBody.stopWatch();
            mMode = 0;
            return true;
        }
        return false;
    }

    private boolean matchString(List<String> data, String target) {
        for (String d : data) {
            if (d.contains(target)) {
                return true;
            }
        }
        return false;
    }

    private int getSeconds(List<String> data) {
        if (matchString(data, "ストップウォッチ")) {
            return 0;
        }
        for (String d : data) {
            int index = d.indexOf("秒");
            if (index > 0) {
                int sindex = index;
                while (sindex > 0) {
                    if (d.charAt(sindex - 1) < '0' && d.charAt(sindex - 1) > '9') {
                        break;
                    }
                    sindex--;
                }
                if (sindex < index) {
                    return Integer.parseInt(d.substring(sindex, index));
                }
            }
        }
        return -1;
    }
}
