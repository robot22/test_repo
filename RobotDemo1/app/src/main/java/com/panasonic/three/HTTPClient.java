package com.panasonic.three;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import android.os.AsyncTask;
import android.util.Log;

public class HTTPClient  {
    private URI url = null;

    public HTTPClient(String uri){
        try {
            url = new URI( uri);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }
    public void post(String msg) {
        new HTTPClientAsyncTask().execute(msg);
    }

    private class HTTPClientAsyncTask extends AsyncTask<String, Integer, Long> {

        protected Long doInBackground(String... msg) {
            HttpPost request = new HttpPost(url);
            try {
                request.setEntity(new StringEntity(msg[0],"UTF-8"));
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }

            DefaultHttpClient httpClient = new DefaultHttpClient();
            try {
                Log.d("HTTPClient", "POST");
                String ret = httpClient.execute(request, new ResponseHandler<String>() {

                    @Override
                    public String handleResponse(HttpResponse response) throws IOException {
                        Log.d(
                                "HTTPClient",
                                "Response: " + response.getStatusLine().getStatusCode()
                        );

                        switch (response.getStatusLine().getStatusCode()) {
                            case HttpStatus.SC_OK:
                                Log.d("HTTPClient", "HTTP Post Succcess.");
                                return "OK";
                            case HttpStatus.SC_NOT_FOUND:
                                Log.d("HTTPClient", "Not Found.");
                                return null;

                            default:
                                Log.d("HTTPClient", "Error occurred.");
                                return null;
                        }

                    }

                });

            } catch (IOException e) {
                Log.d("posttest", "Connection Failed." + e.toString());
            } finally {
                httpClient.getConnectionManager().shutdown();
            }
            return (long)0;
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(Long result) {
            System.out.println("Downloaded " + result + " bytes");
        }
    }
}
