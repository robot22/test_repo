package com.panasonic.three;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.SurfaceTexture;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.opengl.GLES11Ext;
//import android.os.Handler;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.trygle.robotdemo.CameraController;
import com.trygle.robotdemo.GLObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.FloatBuffer;
import java.util.ArrayList;

import javax.microedition.khronos.opengles.GL10;

public class MyCameraController extends CameraController {

    private Context mContext;
    private boolean mAlreadyDetected = false;
    private int stopFlag = 0;
    private int mCount = 0;


    public MyCameraController(Context context) {
        super(context);
        mContext = context;
    }

    /*public void startPreview() {
        mSurfaceTexture = new SurfaceTexture(mTextures[0]);

        try {
            mCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_FRONT);
            Camera.Parameters parameters = mCamera.getParameters();
            parameters.setPreviewSize(640, 480);
            mCamera.setParameters(parameters);

            mCamera.setFaceDetectionListener(faceDetectionListener);
            mCamera.setErrorCallback(new Camera.ErrorCallback() {
                @Override
                public void onError(int error, Camera camera) {
                    Log.d("TEST", "onError: error=" + error);
                }
            });

            mCamera.setPreviewTexture(mSurfaceTexture);
            mCamera.startPreview();
            mCamera.startFaceDetection();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/

    Handler mHandler = new Handler();

    public void startPreview() {
        super.startPreview();
        try {

            mCamera.setFaceDetectionListener(faceDetectionListener);
            mCamera.setErrorCallback(new Camera.ErrorCallback() {
                @Override
                public void onError(int error, Camera camera) {
                    Log.d("TEST", "onError: error=" + error);
                }
            });
            mCamera.startFaceDetection();

            if (true) {
                mCamera.setPreviewCallback(new Camera.PreviewCallback() {
                    public void onPreviewFrame(byte[] data, Camera camera) {

                        if (stopFlag == 1) {
                            mCamera.setPreviewCallback(null);
                            //SurfaceHolder holder = mView.getHolder();
                            //holder.addCallback(null);

                            return;
                        }

                        //mCamera.setPreviewCallback(null);
                        if (mCount == 0) {
                            return;
                        }

                        stopFlag = 1;

                        int previewWidth = camera.getParameters().getPreviewSize().width;
                        int previewHeight = camera.getParameters().getPreviewSize().height;

                        // プレビューデータから Bitmap を生成
                        //Bitmap bmp = getBitmapImageFromYUV(data, previewWidth, previewHeight);
                        byte[] bytes = getJpegImageFromYUV(data, previewWidth, previewHeight);

                        RekoSDK.APICallback callback = new RekoSDK.APICallback() {
                            public void gotResponse(String sResponse) {
                                //ts.cancel();
                                // TESTTEST


                                try {
                                    JSONObject jsonRoot = new JSONObject(sResponse);
                                    // 20150717 hattori add
                                    Log.d("*******************", "response***********************");
                                    Log.i("*******************", jsonRoot.toString() );
                                    // 20150717 hattori add

                                    if (jsonRoot.has("face_detection")) {
                                        Log.d("JSON", jsonRoot.toString());
                                        JSONArray jsonArray = jsonRoot.getJSONArray("face_detection");
                                        int count = 0;
                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            JSONObject json = jsonArray.getJSONObject(i);
                                            if (!json.has("matches")) {
                                                continue;
                                            }
                                            JSONArray jsonArray2 = json.getJSONArray("matches");
                                            JSONObject json2 = jsonArray2.getJSONObject(0);
                                            String score = json2.getString("score");
                                            float value = Float.parseFloat(score);
                                            String msg = "";
                                            if (value > 0.8) {
                                                String tag = json2.getString("tag");
                                                if (tag.equals("shin.hidehiko2")) {
                                                    msg = "しんさん、こんにちは";
                                                }else if(tag.equals("itoh")){
                                                    msg = "いとうさん、こんにちは";
                                                }else if(tag.equals("okimoto")){
                                                    msg = "おきもとさん、こんにちは";
                                                }else if(tag.equals("shin")){
                                                    msg = "しんさん、こんにちは";
                                                }else if(tag.equals("fukui")){
                                                    msg = "ふくいさん、こんにちは";
                                                }else if(tag.equals("hirota")){
                                                    msg = "ひろたさん、こんにちは";
                                                }else {
                                                    msg = "おぼえていたのですが、わすれてしまいました";
                                                }
                                            } else {
                                                msg = "あなたは誰ですか？";
                                            }

                                            final String msg2 = msg;
                                            //Log.d("hogehoge", msg);

                                            mHandler.post(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Toast.makeText(mContext, msg2, Toast.LENGTH_LONG).show();
                                                    //mCamera.stopPreview();
                                                    stopPreview();
                                                }
                                            });

                                        }
                                        RekoSDK.SetRekoData(jsonRoot);// 20150717 hattori add データをセット

                                    }
                                } catch(Exception e) {
                                    Log.v("Exception", e.getMessage());
                                }
                            }
                        };
                        //RekoSDK.face_detect(ImageDataHolder.INSTANCE.getByteArray(), callback);
                        RekoSDK.face_recognize(bytes, "demo_project", "demo_user", 3, callback);

                    }

                });
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void stopPreview() {
        synchronized (this) {
            mCamera.stopFaceDetection();
            super.stopPreview();
        }
    }

    private Camera.FaceDetectionListener faceDetectionListener = new Camera.FaceDetectionListener() {
        boolean mAlreadyDetected = false;
        @Override
        public void onFaceDetection(Camera.Face[] faces, Camera camera) {
            if(mAlreadyDetected){
                return;
            }
            mAlreadyDetected = true;
            final Toast ts = Toast.makeText(mContext, "認識中…", Toast.LENGTH_LONG);
            ts.show();
            //ITOH_ADD_END_20150618
            //Log.d("onFaceDetection", "顔検出数:" + faces.length);
            //ITOH_DELETE_BEGIN_20150618
            //mCameraOverlayView.setFaces(faces);
            //ITOH_DELETE_END_20150618

            /*String TAG = "hogehoge";
            Log.d(TAG, "faces count: " + faces.length);
            for (Camera.Face face : faces) {
                // サポートされていなければ-1が常に返ってくる
                Log.d(TAG, "face id: " + face.id);

                // 顔検出の信頼度 1から100までの値が入っており、100が顔として信頼度が一番高い
                Log.d(TAG, "face score: " + face.score);

                // 検出された顔の範囲
                Log.d(TAG, "face rect: " + face.rect.left + "," + face.rect.top + " - "
                        + face.rect.right + "," + face.rect.bottom);

                // 以下はサポートされていなければnullが入ってくる
                if (face.mouth != null) {
                    Log.d(TAG, "face mouth: " + face.mouth.x + "," + face.mouth.y);
                    Log.d(TAG, "face leftEye: " + face.leftEye.x + "," + face.leftEye.y);
                    Log.d(TAG, "face rightEye: " + face.rightEye.x + "," + face.rightEye.y);
                }
            }*/

            mCount = faces.length;


        }
    };

    public Bitmap getBitmapImageFromYUV(byte[] data, int width, int height) {
        YuvImage yuvimage = new YuvImage(data, ImageFormat.NV21, width, height, null);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        yuvimage.compressToJpeg(new Rect(0, 0, width, height), 80, baos);
        byte[] jdata = baos.toByteArray();
        BitmapFactory.Options bitmapFatoryOptions = new BitmapFactory.Options();
        bitmapFatoryOptions.inPreferredConfig = Bitmap.Config.RGB_565;
        Bitmap bmp = BitmapFactory.decodeByteArray(jdata, 0, jdata.length, bitmapFatoryOptions);
        return bmp;
    }

    public byte[] getJpegImageFromYUV(byte[] data, int width, int height) {
        YuvImage yuvimage = new YuvImage(data, ImageFormat.NV21, width, height, null);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        yuvimage.compressToJpeg(new Rect(0, 0, width, height), 80, baos);
        byte[] jdata = baos.toByteArray();
        return jdata;
    }

}
