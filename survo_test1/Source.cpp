#include <stdio.h>
#include <Windows.h>

#define EXTERN
#include "LeapMotionLestener.h"
#include "leapMotionData.h"

using namespace std;
using namespace System;
using namespace Leap;

/**
下記設計が必要
未：設定ファイルに設定値を全て外出しする。その詳細
未：どの情報をLeapMotionから取得するのか明確化する。
未：ログ出力処理をどうするか。世代管理するか等の運用観点での設計。
対応中：後で変わるので汎用性を持たせる。
*/


// 共通ランタイムのマネージ変数を持つ参照クラス
ref class CommonManage
{
public:
    // 共通ランタイムのシリアルポートクラス
    System::IO::Ports::SerialPort^  SerialPort;

    CommonManage()
	{
		this->SerialPort = gcnew System::IO::Ports::SerialPort();
	};

};

//////////////////////////////////////
//宣言
void InitServo(CommonManage^ commonManage);
bool SetReset(System::IO::Ports::SerialPort^ serialPort, cli::array<unsigned char>^ &response, int id[] );
bool SetNormal(System::IO::Ports::SerialPort^ serialPort, cli::array<unsigned char>^ &response, int id );
bool SetPos(System::IO::Ports::SerialPort^ serialPort, cli::array<unsigned char>^ &response, int id, int pos );
bool SetPos(System::IO::Ports::SerialPort^ serialPort, cli::array<unsigned char>^ &response, System::Collections::Generic::Dictionary<unsigned char, int>^ dataList );
template <typename T>int ArrayLength( T array[] );
//////////////////////////////////////

// サーボモータ側のマネージャなどで持つ

// サーボモータのid一覧
int idList[] = { 0,1,2,3,4,5 };

//// サーボモータの最大・最小角度////
// 舌用
//const int SERVO_ANGLE_MAX = 8000;
//const int SERVO_ANGLE_MIN = -1000; 


const int SERVO_ANGLE_MAX = 4500;
const int SERVO_ANGLE_MIN = -700;

//// タイムアウト時間/////
// INIT(NORMAL, RESET )
const int TIMEOUT_INIT_READ = 100;
const int TIMEOUT_INIT_WRITE = 100;

// SETPOS
const int TIMEOUT_SETPOS_READ = 1;
const int TIMEOUT_SETPOS_WRITE = 1;

// CLOSE
const int TIMEOUT_CLOSE_READ = System::IO::Ports::SerialPort::InfiniteTimeout;// タイムアウトなし
const int TIMEOUT_CLOSE_WRITE = System::IO::Ports::SerialPort::InfiniteTimeout;// タイムアウトなし

// 舌用
const int SERVO_ANGLE_TONGUE_MAX[] = { 0, 4500, 4500, 0, 0 };
const int SERVO_ANGLE_TONGUE_MIN[] = { 0, -700, -700, 0, 0 };

// 手用
const int SERVO_ANGLE_HAND_MAX[] = { 0, 4500, 4500, 4000, 3500 };
const int SERVO_ANGLE_HAND_MIN[] = { 0, -1000, -1200, -1200, -800 };


//////////////////////////////////////


int main()
{

    // LeapMotionからイベントを受け取るキュー
    EventQueue.empty();

    //**** ▼SERVOMOTOR ***********
    // サーボモータの初期化
    CommonManage^ commonManage = gcnew CommonManage();
    InitServo( commonManage );
    //　応答は5バイト
    cli::array<unsigned char>^ response = gcnew cli::array<unsigned char>(5);

    SetReset( commonManage->SerialPort, response, idList);

    for each( int id in idList)
    {
        SetNormal( commonManage->SerialPort, response, id);
    }

    //**** ▲SERVOMOTOR ***********

    //_/_/ ▼LEAPMOTION /_/_/_/_/_/
    //リープモーションのリスナとコントローラーを作成する。

    LeapMotionListener listener;
    Controller controller;
    // Have the sample listener receive events from the controller
    controller.addListener(listener);
    //_/_/ ▲LEAPMOTION /_/_/_/_/_/


    //**** ▼SERVOMOTOR ***********

    float oldYaw = 0;

	commonManage->SerialPort->ReadTimeout = TIMEOUT_SETPOS_READ;
	commonManage->SerialPort->WriteTimeout = TIMEOUT_SETPOS_READ;

    // ※関数化すること
	try{
		while (1)
		{
			if (EventQueue.size() > 0)
			{
				LeapMotionData *data = EventQueue.front();
				float yaw = data->yaw;

				////int fingerNum = ArrayLength(data.fingerAngle);

				int fingerNum = 5;

				System::Collections::Generic::Dictionary<unsigned char, int>^ dataList = gcnew System::Collections::Generic::Dictionary<unsigned char, int>();

				for (int i = 0; i < fingerNum; i++)
				{
					int fingerAngle = data->fingerAngle[i];
					int angleMax = SERVO_ANGLE_HAND_MAX[i];
					int angleMin = SERVO_ANGLE_HAND_MIN[i];

					if (fingerAngle > angleMax)
					{
						fingerAngle = angleMax;
					}
					else if (fingerAngle < angleMin)
					{
						fingerAngle = angleMin;
					}

					dataList->Add(idList[i], fingerAngle);
					printf("%d:%d 、", i, fingerAngle);
				}

				printf("\ncommand send start \n");

				SetPos(commonManage->SerialPort, response, dataList);

				printf("command send end \n");

				try{
					EventQueue.pop();
					free(data);
				}
				catch (System::Exception^ ex)
				{
					printf("***************************");
				}
			}

		}
	}
	finally
	{

		commonManage->SerialPort->ReadTimeout = TIMEOUT_CLOSE_READ;
		commonManage->SerialPort->WriteTimeout = TIMEOUT_CLOSE_WRITE;

		if (commonManage->SerialPort->IsOpen)
		{
			commonManage->SerialPort->Close();
		}

		//_/_/ ▼LEAPMOTION /_/_/_/_/_/
		// リスナを削除する。
		controller.removeListener(listener);
		//_/_/ ▲LEAPMOTION /_/_/_/_/_/

		Sleep(100);
	}
    //**** ▲SERVOMOTOR ***********


    return 0;

}

// サーボモータ側の初期化
// シリアルポートの設定値を設定し接続する。
void InitServo(CommonManage^ commonManage)
{
    commonManage->SerialPort->BaudRate = 1500000;
    commonManage->SerialPort->Parity = System::IO::Ports::Parity::None;
	commonManage->SerialPort->PortName = "COM3";
	commonManage->SerialPort->ReadTimeout = TIMEOUT_INIT_READ;
	commonManage->SerialPort->WriteTimeout = TIMEOUT_INIT_WRITE;
    try{
        commonManage->SerialPort->Open();
    }
    catch(Exception^ ex )
    {
        ex->ToString();
        throw;
    }
}



// サーボモータのステータスをNormalに変更するコマンドを送信する。
// この関数の戻り値がFalseの場合、responseに理由コードが返却される
bool SetNormal(System::IO::Ports::SerialPort^ serialPort, cli::array<unsigned char>^ &response, int id )
{
    // サーボモータのステータスをNormalに変更するコマンドを送信する。
    // コマンドに設定するデータを用意する。
    // ライブラリのWriteSingle関数でコマンドを作成する。
    // 作成したコマンドを、ライブラリのSynchronize関数で送信する。
    // 
    unsigned char^ commandNormal = static_cast<unsigned char>(B3MLib::B3MLib::Options::RunNormal | B3MLib::B3MLib::Options::ControlPosition);

    cli::array<unsigned char>^ data = gcnew cli::array<unsigned char>(1);
    data->SetValue( commandNormal, 0 );

    // コマンド作成
    cli::array<unsigned char>^ command;
    //cli::array<unsigned char>^ response = gcnew cli::array<unsigned char>(4);

    command = 
        B3MLib::B3MLib::WriteSingle(
        0,
        B3MLib::B3MLib::SERVO_TORQUE_ON,
        id,
        data
    );

    // 送信
    return B3MLib::B3MLib::Synchronize(serialPort, command, response);
}

// サーボモータをリセットするコマンドを送信する。
// この関数の戻り値がFalseの場合、responseに理由コードが返却される
bool SetReset(System::IO::Ports::SerialPort^ serialPort, cli::array<unsigned char>^ &response, int id[] )
{
    // サーボモータのステータスをNormalに変更するコマンドを送信する。
    // コマンドに設定するデータを用意する。
    // ライブラリのWriteSingle関数でコマンドを作成する。
    // 作成したコマンドを、ライブラリのSynchronize関数で送信する。
    // 
    cli::array<unsigned char>^ data = gcnew cli::array<unsigned char>(1);

    // コマンド作成
    cli::array<unsigned char>^ command;
    //cli::array<unsigned char>^ response = gcnew cli::array<unsigned char>(4);

    int idMax = ArrayLength(id);
    cli::array<unsigned char>^ idList = gcnew cli::array<unsigned char>( idMax );

	for (int i = 0; i < idMax; i++)
	{
		idList->SetValue(id[i], i);
	}

    command = 
        B3MLib::B3MLib::Reset(
        0, // オプション
        0, // 即時
        idList
    );

    // 送信
    return B3MLib::B3MLib::Synchronize(serialPort, command, response);
}

// サーボモータの回転位置を設定するコマンドを送信する。(シングルモード)
// この関数の戻り値がFalseの場合、responseに理由コードが返却される
bool SetPos(System::IO::Ports::SerialPort^ serialPort, cli::array<unsigned char>^ &response, int id, int pos )
{
    // ライブラリのWriteSingle関数でコマンドを作成する。
    // 作成したコマンドを、ライブラリのSynchronize関数で送信する。

    // コマンド作成
    cli::array<unsigned char>^ command;
    
    command = B3MLib::B3MLib::WriteSingle(
        0,
        B3MLib::B3MLib::SERVO_DESIRED_POSITION,
        id,
        Extensions::Converter::ByteConverter::Int16ToByteArray(pos)
    );

    // 送信
    return B3MLib::B3MLib::Synchronize(serialPort, command, response);

}

// サーボモータの回転位置を設定するコマンドを送信する。(マルチモード)
// この関数の戻り値がFalseの場合、responseに理由コードが返却される
bool SetPos(System::IO::Ports::SerialPort^ serialPort, cli::array<unsigned char>^ &response, System::Collections::Generic::Dictionary<unsigned char, int>^ dataList )
{
    // ライブラリのWriteSingle関数でコマンドを作成する。
    // 作成したコマンドを、ライブラリのSynchronize関数で送信する。

    // コマンド作成
    cli::array<unsigned char>^ command;
    
    command = B3MLib::B3MLib::SetPosision(
        0,
        dataList,
        0
	);

    // 送信
    return B3MLib::B3MLib::Synchronize(serialPort, command, response);

}
// 配列の要素数を求める。
template <typename T>int ArrayLength( T array[] )
{
    return 
        sizeof(array) 
        ? 0
        : (sizeof(array) / sizeof(array[0]));
}