/******************************************************************************\
* Copyright (C) 2012-2014 Leap Motion, Inc. All rights reserved.               *
* Leap Motion proprietary and confidential. Not for distribution.              *
* Use subject to the terms of the Leap Motion SDK Agreement available at       *
* https://developer.leapmotion.com/sdk_agreement, or another agreement         *
* between Leap Motion and you, your company or other organization.             *
\******************************************************************************/

#include <iostream>
#include <cstring>
#include <malloc.h>
#include <stdlib.h>
#include <stdio.h>
#include "LeapMotionLestener.h"
#include "leapMotionData.h"




const std::string fingerNames[] = {"Thumb", "Index", "Middle", "Ring", "Pinky"};
const std::string boneNames[] = {"Metacarpal", "Proximal", "Middle", "Distal"};
const std::string stateNames[] = {"STATE_INVALID", "STATE_START", "STATE_UPDATE", "STATE_END"};

int handId = -1;

/*
//_/_/_/舌用_/_/_/ 
int servoTrims[9]= 
{ -30, -30, -30,
-30, -30, -30,
-30, -30, -30 };

int servoGains[9] =
{ 60, 60, 60,
60, 60, 60,
60, 60, 60 };
//*/


//_/_/_/手用_/_/_/
int servoTrims[]=
{ -50, -50, -50,
-50, -50, -50,
-50, -50, -50 };

int servoGains[] =
{ 40, 40, 40,
40, 35, 40,
40, 40, 40 };
//*/

//int servoGains[9] = {   1, 1, 1, 
//    1, 1, 1, 
//    1, 1, 1 };


void LeapMotionListener::onInit(const Controller& controller) {
    std::cout << "Initialized" << std::endl;
}

void LeapMotionListener::onConnect(const Controller& controller) {
    std::cout << "Connected" << std::endl;
    controller.enableGesture(Gesture::TYPE_CIRCLE);
    controller.enableGesture(Gesture::TYPE_KEY_TAP);
    controller.enableGesture(Gesture::TYPE_SCREEN_TAP);
    controller.enableGesture(Gesture::TYPE_SWIPE);
}

void LeapMotionListener::onDisconnect(const Controller& controller) {
    // Note: not dispatched when running in a debugger.
    std::cout << "Disconnected" << std::endl;
}

void LeapMotionListener::onExit(const Controller& controller) {
    std::cout << "Exited" << std::endl;
}

void LeapMotionListener::onFrame(const Controller& controller) {


    // Get the most recent frame and report some basic information
    const Frame frame = controller.frame();
    HandList hands = frame.hands();

    //std::cout << "Frame id: " << frame.id()
    //          << ", timestamp: " << frame.timestamp()
    //          << ", hands: " << frame.hands().count()
    //          << ", extended fingers: " << frame.fingers().extended().count()
    //          << ", tools: " << frame.tools().count()
    //          << ", gestures: " << frame.gestures().count() << std::endl;
    //if( frame.hands().count() > 0 )
    //{
    //  if( handId == -1 )
    //  {
    //      handId = frame.hands()[0].id();
    //  }
    //  //HandList hands = frame.hands();

    //  Hand hand = frame.hand(handId);

    //  LeapMotionData data;
    //  FingerList fingers = hand.fingers();

    //  for each (Finger finger in fingers)
    //  {
    //      Bone distalBone = finger.bone(Bone::Type::TYPE_DISTAL);
    //      Bone metacarpalBone = finger.bone(Bone::Type::TYPE_METACARPAL);

    //      int fingerType = finger.type();
    //      int fingerAngle = (int)(180 * metacarpalBone.direction().angleTo(distalBone.direction()) / System::Math::PI);
    //      data.fingerAngle[fingerType] = fingerAngle * servoGains[fingerType];
    //      //servoWrite((byte)finger.Type, servoAngles[(int)finger.Type]);
    //      //servoWrite((byte)finger.Type, servoAngle);
    //  }
    //  EventQueue.push(data);

    //}
    //else
    //{
    //    handId = -1;// -1(ハンド無し)を入れておく
    //}

    for (HandList::const_iterator hl = hands.begin(); hl != hands.end(); ++hl) {
        // Get the first hand
        const Hand hand = *hl;
        std::string handType = hand.isLeft() ? "Left hand" : "Right hand";
        const Vector normal = hand.palmNormal();
        const Vector direction = hand.direction();

        /// ▼TESTTEST //////////////////////////////
        LeapMotionData leapMotionData;
        //leapMotionData.HandList = frame.hands();
        //leapMotionData.GestureList = frame.gestures();
        if( frame.hands().count() > 0 )
        {
            leapMotionData.yaw = direction.yaw() * RAD_TO_DEG;
        }
        /// ▲TESTTEST //////////////////////////////


        // 指の情報を取得
        const FingerList fingers = hand.fingers();

        for (FingerList::const_iterator fl = fingers.begin(); fl != fingers.end(); ++fl)
        {
            const Finger finger = *fl;

            Bone distalBone = finger.bone(Bone::Type::TYPE_DISTAL);
            Bone metacarpalBone = finger.bone(Bone::Type::TYPE_METACARPAL);

            int fingerType = finger.type();
            int fingerAngle = (int)((180 * metacarpalBone.direction().angleTo(distalBone.direction())) / System::Math::PI);
			leapMotionData.fingerAngle[fingerType] = (fingerAngle + servoTrims[fingerType])*servoGains[fingerType];
        }
		if (frame.hands().count() > 0)
		{
			// とりあえずキューは100個まで
			int queueNum = EventQueue.size();
			if (queueNum < 100)
			{
				// キューへ格納
				LeapMotionData *pLeapMotionData = (LeapMotionData*)malloc(sizeof(LeapMotionData));
				memcpy(pLeapMotionData, &leapMotionData, sizeof(LeapMotionData));
				EventQueue.push(std::move(pLeapMotionData));
			}
		}
    }

}

void LeapMotionListener::onFocusGained(const Controller& controller) {
    std::cout << "Focus Gained" << std::endl;
}

void LeapMotionListener::onFocusLost(const Controller& controller) {
    std::cout << "Focus Lost" << std::endl;
}

void LeapMotionListener::onDeviceChange(const Controller& controller) {
    std::cout << "Device Changed" << std::endl;
    const DeviceList devices = controller.devices();

    for (int i = 0; i < devices.count(); ++i) {
        std::cout << "id: " << devices[i].toString() << std::endl;
        std::cout << "  isStreaming: " << (devices[i].isStreaming() ? "true" : "false") << std::endl;
    }
}

void LeapMotionListener::onServiceConnect(const Controller& controller) {
    std::cout << "Service Connected" << std::endl;
}

void LeapMotionListener::onServiceDisconnect(const Controller& controller) {
    std::cout << "Service Disconnected" << std::endl;
}

//int main(int argc, char** argv) {
//  // Create a sample listener and controller
//  LeapMotionListener listener;
//  Controller controller;
//
//  // Have the sample listener receive events from the controller
//  controller.addListener(listener);
//
//  if (argc > 1 && strcmp(argv[1], "--bg") == 0)
//    controller.setPolicy(Leap::Controller::POLICY_BACKGROUND_FRAMES);
//
//  // Keep this process running until Enter is pressed
//  std::cout << "Press Enter to quit..." << std::endl;
//  std::cin.get();
//
//  // Remove the sample listener when done
//  controller.removeListener(listener);
//
//  return 0;
//}
