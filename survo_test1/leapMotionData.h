#ifndef LEAPMOTIONDATA
#define LEAPMOTIONDATA

#include <queue>

struct LeapMotionData
{

public:
    LeapMotionData()
    {
    };

    int Gesture;
    int PosX;
    int PosY;
    float yaw;

	int fingerAngle[5];

    //// ハンドリスト
    HandList HandList;

    //// ジェスチャーリスト
    GestureList GestureList;
    

};

#ifndef EXTERN
#define EXTERN extern
#endif
EXTERN std::queue<LeapMotionData*> EventQueue;

#endif


